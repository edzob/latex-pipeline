# syntax=docker/dockerfile:1
FROM texlive/texlive:latest

RUN apt-get update;
# RUN apt-get upgrade -y;
RUN apt-get install -y fonts-open-sans;
RUN chmod -R o+w /usr/local/texlive/2024;
RUN fc-cache --really-force;
