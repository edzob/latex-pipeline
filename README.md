![Pipeline status](https://gitlab.com/martisak/latex-pipeline/badges/master/pipeline.svg)

# Gitlab CI pipeline for LaTeX example

This repository contains examples for the blog post 
[How to annoy your co-authors: a Gitlab CI pipeline for LaTeX](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/).

This latex-pipeline is using 
[Docker](https://docs.docker.com/get-docker/) 
and 
[GNU make](https://www.gnu.org/software/make/)
together with 
[latexMK](https://ctan.org/pkg/latexmk?lang=en) 
in a the 
[texlive:latest container](https://hub.docker.com/r/texlive/texlive).

The [texlive:latest container](https://hub.docker.com/r/texlive/texlive).
is updated weekly by the texlive organisation.

When you need to change the (advanced) setting, you can do this via the `Makefile` and `.latexmk` files.

---

Compile LaTeX locally with the command:

```bash
make clean render
# OR
make clean renders
```

or to keep compiling, the LaTeX code into the pdf, when the input files are updated:


```bash
make clean render LATEXMK_OPTIONS_EXTRA=-pvc
# OR
make clean renders LATEXMK_OPTIONS_EXTRA=-pvc
```

*Note*: make render**s** is making use of a dockercontainer for the latex environment, if you do not want to use docker and want to use your local latex environment you can use the render command. The gitlab pipeline is using the same dockercontainer to generate the pdf.

---

To refresh the PDF continuous in the PDF viewer [Evince](https://wiki.gnome.org/Apps/Evince)  :
```bash
evince paper/latexmk/main.pdf
```

---

## Example setup
* A setup could be to run (arch) linux on your system, with [Kate](https://kate-editor.org/) as editor, and [Evince](https://wiki.gnome.org/Apps/Evince) as pdf viewer both titled on your screen.
     * alternative you could use various text editors as [vim](https://github.com/vim/vim) ([tutorial](https://www.freecodecamp.org/news/vim-beginners-guide/), [vi stack exchange](https://vi.stackexchange.com/)), [neovim](https://neovim.io/), emacs ([tutorial](https://www.redhat.com/en/blog/beginners-guide-emacs), [emacs stack exchange](https://emacs.stackexchange.com/)), sublime, vs-code etc
     * the local toolchain is lacking a good grammar tool. Kate (or any other local linux editor) does support spellchecking. A way forward could be to include Language tool as docker container and use this as external LanguageTool to Kate (or any other local linux editor).

## Combine GitLab with Overleaf
* see [this wiki page](https://gitlab.com/edzob/antifragile-research/-/wikis/docs/Overleaf-and-Gitlab) on the Antifragile MSc wiki on how to configure Gitlab and Overleaf into the same project folder on your laptop.
     * This will enable you to combine the realtime collaboration in Overleaf, as local development, for the moments that you do not have internet or overleaf is not behaving.
     * This will also enable a backup of your files in overleaf onto your local machines (git ditribution) as on the gilbar server. Do know that you need a paid (student) subscription with overleaf to have git access to your overleaf projects.
